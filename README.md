# Checking certificate expire date

## Installation

```sh
git clone https://gitlab.com/beplay/devops/tools/query-cert-exp.git
cd query-cert-exp

./get-cert-exp.sh google.com
```

## Usage

```sh
Usage: ./get-cert-exp.sh [option] 
   -f, --file <filename> Filename contains domain list
   -n, --name <domain> Domain name
   -h, --help 
```

### Scenario 1: Single domain query

```sh
./get-cert-exp.sh google.com
```

### Scenario 2: Query by list of domains in file

Let's define example `domains.txt` with domain list

```txt
microsoft.com
example.com
```

Then query with input file

```sh
./get-cert-exp.sh -f ./domains.txt
```

---

## Result will be print to console

```json
{"domain":"microsoft.com", "status": "online", "expDate":"2022-02-10", "remainDays":239}
{"domain":"example.com", "status": "offline"}
```
