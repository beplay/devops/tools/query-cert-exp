#!/bin/bash

################################################
# Script for query certificate expire date
# Author: Chitchai Changpradist
################################################

# Print certificate expire
print() {
  # name="mooc.truecorp.co.th"
  local name=$1

  # Checking domain is available
  nc -zv -w1 $name 443 2> /dev/null
  
  if [[ 0 -eq $? ]]; then
    local rawExpDate=$(echo | openssl s_client -showcerts -servername $name -connect $name:443 2>/dev/null | openssl x509 -inform pem -noout -enddate | cut -d "=" -f 2)
    local nowEpoch=$( date +%s )
    local expEpoch=$( date -d "$rawExpDate" +%s )
    local expDate=$( date -d "$rawExpDate" +%Y-%m-%d )
    local expDays="$(( ($expEpoch - $nowEpoch) / (3600 * 24) ))"

    echo "{\"domain\":\"$name\", \"status\": \"online\", \"expDate\":\"$expDate\", \"remainDays\":$expDays}"
  else
    echo "{\"domain\":\"$name\", \"status\": \"offline\"}" 
  fi
}

####################
# Print usage
####################
printUsage() {
  echo -e "\nUsage: $0 [option] "
  echo -e "\t -f, --file <filename> Filename contains domain list"
  echo -e "\t -n, --name <domain> Domain name"
  echo -e "\t -h, --help "
  echo -e "\n"
  exit 1
}


####################
# Main
####################
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -f|--file)
    FILENAME="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--name)
    DOMAINNAME="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    printUsage
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ -n ${FILENAME} ]]; then
  for name in $(cat ${FILENAME}); 
  do 
    print $name; 
  done
elif [[ -n $DOMAINNAME ]]; then
  print $DOMAINNAME
elif [[ -n $1 ]]; then
  print $1
else
  printUsage
fi


